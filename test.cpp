///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file test.cpp
/// @version 1.0
///
/// Contains unit tests for functions
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   29_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "cat.hpp"

using namespace std;

int main() {
   CatEmpire empire;
   // test empty function
   cout << "Tree empty: " << boolalpha << empire.empty() << endl;

   // generate test cats
   Cat::initNames();
   Cat* cat1 = Cat::makeCat();
   Cat* cat2 = Cat::makeCat();
   Cat* cat3 = Cat::makeCat();
   Cat* cat4 = Cat::makeCat();

   // test addCat
   empire.addCat( cat1 );
   empire.addCat( cat2 );
   empire.addCat( cat3 );
   empire.addCat( cat4 );
   cout << "Tree empty: " << boolalpha << empire.empty() << endl;

}
