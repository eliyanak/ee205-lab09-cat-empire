///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   29_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}


void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsInorderReverse( topCat, 1 );
}


void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsInorder( topCat );
}


void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsPreorder( topCat );
}


bool CatEmpire::empty() {
   if (topCat == nullptr) {
      return true;
   } else {
      return false;
   }
}


void CatEmpire::addCat( Cat* newCat ) {
   // insert newCat at root if it is empty
   if( topCat == nullptr) {
      //cout << "CAT INSERTED AT ROOT" << endl;
      topCat = newCat;
      return;
   }
   // if newCat's name precedes topCat's name alphabetically, insert it into the left branch
   else if( newCat->name < topCat->name ) {
      //cout << "NEW CAT GOES TO THE LEFT" << endl;
      // if the left branch is empty, insert newCat
      if( topCat->left == nullptr ) {
         //cout << "ROOT'S LEFT BRANCH EMPTY, CAT INSERTED" << endl;
         topCat->left = newCat;
         return;
      } else {
         // call 2nd addCat() function to move on to the other branches
         //cout << "ROOT'S LEFT BRANCH OCCUPIED, ADDCAT CALLED AGAIN" << endl;
         addCat( topCat->left, newCat );
         return;
      }
   }
   // else if newCat follows topCat alphabetically
   else if( newCat->name > topCat->name ) {
      //cout << "NEW CAT GOES TO THE RIGHT" << endl;
      // if right branch is empty, insert newCat
      if( topCat->right == nullptr ) {
         //cout << "ROOT'S RIGHT BRANCH EMPTY, CAT INSERTED" << endl;
         topCat->right = newCat;
         return;
      }
      // otherwise call 2nd addCat() function to move on to next branch
      else {
         //cout << "ROOT'S RIGHT BRANCH OCCUPIED, ADDCAT CALLED AGAIN" << endl;
         addCat( topCat->right, newCat );
         return;
      }
   }
}


void CatEmpire::addCat( Cat* atCat, Cat* newCat ) {
   // if newCat precedes atCat
   if( newCat->name < atCat->name ) {
      //cout << "NEWCAT GOES TO THE LEFT OF ATCAT" << endl;
      // if left branch is empty, insert newCat
      if( atCat->left == nullptr ) {
         //cout << "ATCAT LEFT BRANCH EMPTY, CAT INSERTED" << endl;
         atCat->left = newCat;
      }
      // otherwise call addCat() again
      else {
         //cout << "ATCAT LEFT BRANCH OCCUPIED, ADDCAT CALLED AGAIN" << endl;
         addCat( atCat->left, newCat );
      }
   }
   // otherwise if newCat follows atCat
   if( newCat->name > atCat->name ) {
      //cout << "NEWCAT GOES TO THE RIGHT OF ATCAT" << endl;
      // if right branch is empty, insert newCat
      if( atCat->right == nullptr ) {
         //cout << "ATCAT RIGHT BRANCH EMPTY, CAT INSERTED" << endl;
         atCat->right = newCat;
      }
      // otherwise call addCat() again
      else {
         //cout << "ATCAT RIGHT BRANCH OCCUPIED, ADDCAT CALLED AGAIN" << endl;
         addCat( atCat->right, newCat );
      }
   }
}


void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const {
   if( atCat == nullptr ) {
      return;
   }
   CatEmpire::dfsInorderReverse( atCat->right, depth+1 );
   cout << string( 6 * (depth-1), ' ' ) << atCat->name;
   // node is a leaf node
   if( (atCat->left == nullptr) && (atCat->right == nullptr) ) {
      cout << endl;
   } 
   // node has both a left and right child
   else if( (atCat->left != nullptr) && (atCat->right != nullptr) ) {
      cout << "<" << endl;
   } 
   // node has a left child
   else if( (atCat->left != nullptr) && (atCat->right == nullptr) ) {
      cout << "\\" << endl;
   }
   // node has a right child
   else if( (atCat->left == nullptr) && (atCat->right != nullptr) ) {
      cout << "/" << endl;
   }
   CatEmpire::dfsInorderReverse( atCat->left, depth+1 );
   return;
}


void CatEmpire::dfsInorder( Cat* atCat ) const {
   if( atCat == nullptr ) {
      return;
   }
   CatEmpire::dfsInorder( atCat->left );
   cout << atCat->name << endl;
   CatEmpire::dfsInorder( atCat->right );
   return;
}


void CatEmpire::dfsPreorder( Cat* atCat ) const {
   if( atCat == nullptr ) {
      return;
   }
   // node has left and right child
   if( (atCat->left != nullptr) && (atCat->right != nullptr) ) {
      cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
   }
   // node has left child
   else if( (atCat->left != nullptr) && (atCat->right == nullptr) ) {
      cout << atCat->name << " begat " << atCat->left->name << endl;
   }
   // node has right child
   else if( (atCat->left == nullptr) && (atCat->right != nullptr) ) {
      cout << atCat->name << " begat " << atCat->right->name << endl;
   }
   CatEmpire::dfsPreorder( atCat->left );
   CatEmpire::dfsPreorder( atCat->right );
   return;
}


void CatEmpire::getEnglishSuffix( int n ) const {
   // if tens digit is 1
   if( (n % 100 >= 10) && (n % 100 < 20) ) {
      cout << n << "th";
   } 
   // otherwise look at the ones digit
   else if( n % 10 == 1 ) {
      cout << n << "st";
   } else if ( n % 10 == 2 ) {
      cout << n << "nd";
   } else if ( n % 10 == 3 ) {
      cout << n << "rd";
   } else {
      cout << n << "th";
   }
}


void CatEmpire::catGenerations() const {
   int depth = 1;
   CatEmpire::getEnglishSuffix( depth );
   cout << " Generation" << endl;
   queue<Cat*> catQueue;
   catQueue.push(topCat);
   catQueue.push(nullptr);
   while( !catQueue.empty() ) {
      Cat* cat = catQueue.front();
      catQueue.pop();
      if( cat == nullptr ) {
         depth++;
         catQueue.push(nullptr);
         if( catQueue.front() == nullptr ) {
            break;
         } else {
            cout << endl;
            CatEmpire::getEnglishSuffix( depth );
            cout << " Generation" << endl;
            continue;
         }
      }
      if( cat->left != nullptr ) {
         catQueue.push( cat->left );
      }
      if( cat->right != nullptr ) {
         catQueue.push( cat->right );
      }

      cout << "  " << cat->name;
   }
   cout << endl;
}
